from django.contrib import admin
from .models import *
# Register your models here.
models = [DemoPage, DemoProduct, DemoUser, SandboxSetting]
admin.site.register(models)
