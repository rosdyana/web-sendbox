from enum import Enum, unique
from django.db import models


@unique
class EventActionType(Enum):
    ADD_TO_CART = "AddToCart"
    SORT = "Sort"
    CHECKOUT = "Checkout"
    REFUND = "Refund"
    CLICK_PRODUCT = "ClickProduct"
    CLICK_CHECKOUT = "ClickCheckout"
    CLICK_FOOTER = "ClickFooter"
    COMPLETE_REGISTRATION = "CompleteRegistration"
    CLICK_MENU = "ClickMenu"
    VIEW_CONTENT = "ViewContent"
    PURCHASE = "Purchase"
    REMOVE_FROM_CART = "RemoveFromCart"
    CLICK_PURCHASE = "ClickPurchase"
    CHECKOUT_OPTION = "CheckoutOption"
    FILTER = "Filter"
    TRACKING_PIXEL = "TrackingPixel"
    TRACKING_LINK = "TrackingLink"
    UNSUBSCRIBE = "Unsubscribe"
    RESUBSCRIBE = "Resubscribe"
    PAGEVIEW = "Pageview"


class SandboxSetting(models.Model):
    beacon_host = models.URLField(unique=True)
    tracking_id = models.CharField(max_length=25)

    def __str__(self):
        return f"{self.beacon_host} - {self.tracking_id}"


class DemoUser(models.Model):
    name = models.CharField(max_length=25)
    email = models.EmailField()
    member_sn = models.CharField(max_length=25, unique=True)

    def __str__(self):
        return f"{self.name} - {self.email} - {self.member_sn}"


class DemoProduct(models.Model):
    id_product = models.CharField(max_length=25, unique=True)
    name = models.CharField(max_length=25)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    category = models.CharField(max_length=25)
    variant = models.CharField(max_length=25)
    url = models.URLField()

    def __str__(self):
        return f"{self.name} - {self.url}"


class DemoPage(models.Model):
    name = models.CharField(max_length=25)
    url = models.URLField(unique=True)

    def __str__(self):
        return f"{self.name} - {self.url}"