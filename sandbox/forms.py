from django import forms
from django.forms import ModelForm
from django.utils import timezone
from tempus_dominus.widgets import DateTimePicker
from .models import DemoUser, EventActionType, DemoProduct, DemoPage, SandboxSetting

class ActionForm(forms.Form):
    config = forms.ModelChoiceField(queryset=SandboxSetting.objects.all(), label="Configuration", help_text="Select Configuration to perform action.")
    user = forms.ModelChoiceField(queryset=DemoUser.objects.all(), label='Audience', help_text='Select user to perform action.')
    event = forms.ChoiceField(choices=[(tag, tag.value) for tag in EventActionType], help_text='Select action to perform action.')
    product = forms.ModelChoiceField(required=False, queryset=DemoProduct.objects.all(), help_text='Select product to perform action.')
    page = forms.ModelChoiceField(required=False, label='Page', queryset=DemoPage.objects.all(), help_text='Select page to perform action.')
    datetime = forms.DateTimeField(required=False, label='Date Time', widget=DateTimePicker(
        options={
                'useCurrent': True,
                'collapse': False,
                'defaultDate': timezone.now().isoformat()
                },
                attrs={
                    'append': 'fa fa-calendar',
                    'icon_toggle': True},), help_text='Select datetime to perform action.')


class AudienceForm(ModelForm):
    member_sn = forms.CharField(required=True)

    class Meta:
        model = DemoUser
        fields = ['name', 'email', 'member_sn']


class ProductForm(ModelForm):
    id_product = forms.CharField(required=True)

    class Meta:
        model = DemoProduct
        fields = ['id_product', 'name', 'price', 'category', 'variant', 'url']


class PageForm(ModelForm):
    url = forms.CharField(required=True)

    class Meta:
        model = DemoPage
        fields = ['name', 'url']


class SettingsForm(ModelForm):
    class Meta:
        model = SandboxSetting
        fields = ['beacon_host', 'tracking_id']