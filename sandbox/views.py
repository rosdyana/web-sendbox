from django.shortcuts import render
from django.contrib import messages
from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.utils.dateparse import parse_datetime
from django.utils import timezone
import requests
import datetime
from .models import *
from .forms import ActionForm, AudienceForm, ProductForm, PageForm, SettingsForm


ecommerce_event = [EventActionType.ADD_TO_CART.value,
                   EventActionType.PURCHASE.value,
                   EventActionType.REMOVE_FROM_CART.value,
                   EventActionType.CLICK_PURCHASE.value,
                   EventActionType.CLICK_PRODUCT.value]

def send_beacon(user, config=None, queue_time=None, event=None, product=None, page=None):
    

    t = 'event'
    if event == 'Pageview':
        t = 'pageview'

    dl = None
    dt = None
    if product:
        dl = product.url
        dt = product.name
    elif page:
        dl = page.url
        dt = page.name
    tid = f'{settings.TRACKING_ID}'
    host = f'{settings.BEACON_HOST}'
    if config:
        tid = config.tracking_id
        host = config.beacon_host

    headers = {
        'authority': host,
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36',
        'accept': 'image/webp,image/apng,image/*,*/*;q=0.8',
        'sec-fetch-site': 'cross-site',
        'sec-fetch-mode': 'no-cors',
        # 'referer': 'https://www.rubys.com.tw/product_category.php?store_type_sn=41&category_sn=1208',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'en-US,en;q=0.9,id;q=0.8,vi;q=0.7',
    }

    params = (
        ('cu', 'TWD'),
        ('_s', '1'),
        ('dl', f'{dl}'),
        ('ul', 'en-us'),
        ('de', 'UTF-8'),
        ('dt', f'{dt}'),
        ('sd', '24-bit'),
        ('sr', '1920x1080'),
        ('vp', '952x491'),
        ('je', '0'),
        ('tid', tid),
        ('uid', f'{user.member_sn}'),
        ('v', '1'),
        ('t', f'{t}'),
    )

    if product:
        params += (('pr1id', product.id_product), ('pr1nm', product.name),)

    if event != 'Pageview':
        if event in ecommerce_event:
            params += (('ec', 'Ecommerce'), ('ea', event),)
        else:
            params += (('ec', event), ('ea', event),)

        if event == EventActionType.PURCHASE.value:
            params += (('tr', product.price),)

    if queue_time:
        params += (('qt', queue_time),)

    print(params)

    response = requests.get(f'{host}/collect', headers=headers, params=params)


def perform_action(request):
    form = ActionForm()
    if request.method == "POST":
        form = ActionForm(data=request.POST)
        if form.is_valid():
            print(form.data)
            event = form.data.get('event', None)
            if event:
                event = EventActionType[event.split('.')[1]].value
            now = timezone.now()
            selected_time = parse_datetime(form.data['datetime'])
            _now = datetime.datetime(now.year, now.month, now.day, now.hour, now.minute, now.second)
            queue_time = 0
            if selected_time:
                _delay = datetime.datetime(selected_time.year, selected_time.month, selected_time.day, selected_time.hour, selected_time.minute, selected_time.second)
                queue_time = str((_now - _delay).seconds * 10000)
            user = DemoUser.objects.get(id=form.data['user'])
            product = form.data.get('product', None)
            page = form.data.get('page', None)
            if product:
                product = DemoProduct.objects.get(id=product)
            if page:
                page = DemoPage.objects.get(id=page)
            config = form.data.get('config', None)
            if config:
                config = SandboxSetting.objects.get(id=config)
            send_beacon(user, config=config, event=event, page=page, queue_time=queue_time, product=product)
            return redirect(reverse("perform-action"))
    return render(request, "action.html", context={"form": form})


def index(request):
    """ index page """
    return render(request, "index.html")


def audience_view(request):
    form = AudienceForm()
    if request.method == "POST":
        form = AudienceForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse("audience-view"))
    return render(request, "audience.html", context={"form": form, "audiences": DemoUser.objects.all()})


def product_view(request):
    form = ProductForm()
    if request.method == "POST":
        form = ProductForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse("product-view"))
    return render(request, "product.html", context={"form": form, "products": DemoProduct.objects.all()})


def page_view(request):
    form = PageForm()
    if request.method == "POST":
        form = PageForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse("page-view"))
    return render(request, "page.html", context={"form": form, "pages": DemoPage.objects.all()})


def settings_view(request):
    form = SettingsForm()
    if request.method == "POST":
        form =SettingsForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse("settings-view"))
    return render(request, "settings.html", context={"form": form, "settings": SandboxSetting.objects.all()})


def delete_product(request):
    if request.method == "POST":
        data = request.POST
        product_id = data.get("id")

        product = get_object_or_404(DemoProduct, pk=product_id)

        product.delete()

        return HttpResponse(status=200)


def delete_page(request):
    if request.method == "POST":
        data = request.POST
        page_id = data.get("id")

        page = get_object_or_404(DemoPage, pk=page_id)

        page.delete()

        return HttpResponse(status=200)


def delete_audience(request):
    if request.method == "POST":
        data = request.POST
        audience_id = data.get("id")

        audience = get_object_or_404(DemoUser, pk=audience_id)

        audience.delete()

        return HttpResponse(status=200)