#!/bin/sh

# wait for PSQL server to start
./wait-for-it.sh pg:5432 -t 30

# double check python dependencies
cd /code/ && /root/site/bin/pip3 install -r requirements.txt

# migrate db, so we have the latest db schema
cd /code/ && /root/site/bin/python manage.py migrate

cd /code/ && /root/site/bin/python manage.py runserver 0.0.0.0:8000
