function send_ajax(data, url, callback) {
	$.ajax({
		type : "POST",
        url : url,
        data: data,
		success: callback,
		error: function(error) {
            console.log("Error doing ajax request...");
            console.log(error);
		}
	});
}


function delete_product(id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var data ={"id": id, "csrfmiddlewaretoken": csrftoken};
    send_ajax(data, "/delete-product", function(){
        $("#"+id).hide("fast", function(){ $(this).remove(); })
    });
}

function delete_page(id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var data ={"id": id, "csrfmiddlewaretoken": csrftoken};
    send_ajax(data, "/delete-page", function(){
        $("#"+id).hide("fast", function(){ $(this).remove(); })
    });
}


function delete_audience(id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var data ={"id": id, "csrfmiddlewaretoken": csrftoken};
    send_ajax(data, "/delete-audience", function(){
        $("#"+id).hide("fast", function(){ $(this).remove(); })
    });
}