"""websandbox URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from sandbox.views import *

urlpatterns = [
    path('', index, name="index"),
    path('admin/', admin.site.urls),
    path('action/', perform_action, name="perform-action"),
    path('audience/', audience_view, name="audience-view"),
    path('product/', product_view, name="product-view"),
    path('page/', page_view, name="page-view"),
    path('settings/', settings_view, name="settings-view"),
    path('delete-product', delete_product, name="delete-product"),
    path('delete-page', delete_page, name="delete-page"),
    path('delete-audience', delete_audience, name="delete-audience"),
]
