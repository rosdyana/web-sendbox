FROM python:3.7-alpine
ENV PYTHONUNBUFFERED 1

ARG PIP3="/root/site/bin/pip3"

RUN python3.7 -m venv /root/site

RUN apk update --no-cache && \
    apk add --virtual \
    .build-deps \
    build-base \
    postgresql-client \
    postgresql-dev \
    nano \
    bash \
    musl-dev \
    libffi-dev \
    libpng-dev \
    jpeg-dev \
    zlib-dev \
    gcc

RUN ${PIP3} install -U pip
RUN ${PIP3} install \
    django \
    requests \
    django-tempus-dominus \
    psycopg2-binary